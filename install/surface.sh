#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

echo "wait for reflector to do it's to get faster mirrors"
sudo reflector -c Australia -a 12 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy
git clone https://aur.archlinux.org/paru.git
cd paru/
makepkg -si --noconfirm

cd
paru -S --noconfirm clipmenu-git
paru -S --noconfirm cointop-bin
paru -S --noconfirm colorpicker-ym1234-git
paru -S --noconfirm devour
paru -S --noconfirm google-chrome
paru -S --noconfirm pipewire-jack-dropin
paru -S --noconfirm pirate-get
paru -S --noconfirm pmount
paru -S --noconfirm pup
paru -S --noconfirm rbw
paru -S --noconfirm udiskie-dmenu-git
paru -S --noconfirm urlview

paru -S --noconfirm alacritty bat clipmenu-git cmus efibootmgr exa expac fakeroot feh ffmpegthumbnailer flex foliate fzf gcc gimp highlight imagemagick jq make mpc mpd mpv ncmpcpp neovim nnn notmuch p7zip pamixer patch pavucontrol picom pipewire-alsa pipewire-pulse pkgconf pulseaudio-alsa pulsemixer qutebrowser ranger recode rsync scrot sudo transmission-cli trash-cli ttf-font-awesome ueberzug unrar unzip wget xclip xev xdg-user-dirs xdotool xorg-server xorg-xbacklight xorg-xinit xorg-xinput xorg-xkill xorg-xsetroot youtube-dl zathura-pdf-poppler zsh zsh-autosuggestions zsh-syntax-highlighting 

rm -R .config
git clone https://quaversa:t440x220@github.com/quaversa/dots.git && cd dots
cp -R {.cache,.config,.icons,.local,.themes,.vim,.fzf-marks,.xinitrc,.Xresources,.zsh_aliases,.zshrc} /home/me/

cd /home/me/.config/suckless/dmenu-5.0/ && sudo make install
cd /home/me/.config/suckless/dwm-6.2/ && sudo make install
cd /home/me/.config/suckless/paleofetch/ && sudo make install
cd /home/me/.config/suckless/st/ && sudo make install
cd /home/me/.config/suckless/slock-1.4/ && sudo make install
cd /home/me/.config/xmenu/ && sudo make install

cd /home/me/dots/postinstall/
sudo mv /etc/systemd/logind.conf /etc/systemd/logind.confbak 
sudo cp logind.conf /etc/systemd/
sudo mkdir /etc/systemd/system/getty@tty1.service.d
sudo cp overide.conf /etc/systemd/system/getty@tty1.service.d/
sudo cp {00-keyboard.conf,30-touchpad.conf} /etc/X11/xorg.conf.d/
sudo cp vconsole.conf /etc/

cd
git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
git clone https://github.com/NullSense/fuzzy-sys.git
unzip fuzzy-sys-master.zip
sudo cp fuzzy-sys-master/fuzzy-sys.plugin.zsh /usr/share/zsh/plugins/
sudo chsh -s /bin/zsh root
chsh -s /bin/zsh me
sudo cp 31_hold_shift /etc/grub.d/ 
sudo chmod +x /etc/grub.d/31_hold_shift

echo "$(whoami)"
[ "$UID" -eq 0 ] || exec sudo "$0" "$@"
sudo echo "GRUB_FORCE_HIDDEN_MENU=true" >> /etc/default/grub
sudo echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg

exit
echo "done.. maybe reboot"
