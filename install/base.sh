#!/bin/bash

ln -sf /usr/share/zoneinfo/Australia/Sydney /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "rch48t" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 rch48t.localdomain rch48t" >> /etc/hosts
echo root:password | chpasswd

pacman -S --noconfirm grub os-prober acpid networkmanager dialog wpa_supplicant dosfstools reflector base-devel tlp tlp-rdw intel-ucode xf86-video-intel xdg-user-dirs xdg-utils bluez bluez-utils rsync reflector terminus-font vim realtime-privileges

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable tlp 
systemctl enable NetworkManager
systemctl enable NetworkManager-dispatcher.service
systemctl enable bluetooth
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable acpid

useradd -m me
echo me:password | chpasswd
usermod -aG wheel,audio,realtime me

echo "me ALL=(ALL) ALL" >> /etc/sudoers.d/me
echo "me ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/me
