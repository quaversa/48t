# initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

# Add .local/bin to PATH
if [ -d "$HOME/.local/bin" ]; then;
		PATH="$HOME/.local/bin:$PATH"
fi

export CDPATH=.:~/:/media:~/.config:~/.config/suckless:~/.local

PS1="%~ %{$fg[red]%}%{$reset_color%}$%b "

HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000

autoload -U colors && colors
autoload -U compinit

setopt HIST_VERIFY
setopt SHARE_HISTORY
setopt HIST_IGNORE_SPACE
setopt HIST_SAVE_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt EXTENDED_GLOB
setopt GLOB_COMPLETE

export KEYTIMEOUT=1
export EDITOR="nvim"
export VISUAL="nvim"
export SUDO_EDITOR="nvim"
export TERMINAL="st"
export BROWSER=/usr/bin/firefox
export READER="zathura"
export FILE="ranger"
export FZF_DEFAULT_COMMAND="find -L -maxdepth 4"
export BW_SESSION="n3oDG6qHGZbx9nFXYfYR4GL+llGIn+NmJUwb580B2bcut7Y8Rxct5aNvB34UhPZKqzodRPSkrc1TbZuRIn+OSw=="
export HISTIGNORE="ytfzf*:yt*:youtube-dl*"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export DEFAULT_RECIPIENT="quaoub@gmail.com"
export CM_LAUNCHER='dmenu'
export CM_MAX_CLIPS=24
export NNN_PLUG='c:togglex;d:dragdrop;f:fzcd;m:nmount;l:preview-tui;s:suedit;u:upload;w:wallpaper;x:xdgdefault'
export NNN_FCOLORS='0000E6310000000000000000'
export NNN_FIFO="/tmp/nnn.fifo"
export NNN_TRASH=1
export NNN_MCLICK='.'
export LC_COLLATE="C"
export NNN_BMS='a:~/.config/herbstluftwm;b:~/.local/bin;c:~/.config;d:~/Downloads;g:~/Dots/48t/dots/.config;h:~/;j:~/Documents/jots;n:~/.config/nnn;p:~/Pictures;s:~/.config/suckless;t:~/Templates;u:/media;v:~/Videos'
#export SDCV_PAGER='less --quit-if-one-screen -RX'

function def() {
	sdcv -n --utf8-output --color "$@" 2>&1 | \
	fold --width=$(tput cols) | \
	less --quit-if-one-screen -RX
}

function expand-alias() { #expand-alias
	zle _expand_alias
	zle self-insert
}
zle -N expand-alias

cd() # cd and ls after
{
	builtin cd "$@" && command ls --color=always
}


case $TERM in #dynamic terminal title
  (*st* | alacritty)
    function precmd {
      print -Pn "\e]0; %(1j,%j job%(2j|s|); ,)%~\a"
    }
    function preexec {
      printf "\033]0;%s\a" "$1"
    }
  ;;
esac

bindkey -- '^e'    end-of-line
bindkey -- '^b'    beginning-of-line

# initialize completion
compinit -u -d "$compfile"

##aliases
. ~/.config/zsh/zsh_aliases

##fzf completion
source ~/.config/zsh/fzf-tab.plugin.zsh
[ -f ~/.config/zsh/fzf.zsh ] && source ~/.config/zsh/fzf.zsh
#autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
#zsh-syntax-highlighting; should be last
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
