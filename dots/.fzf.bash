# Setup fzf
# ---------
if [[ ! "$PATH" == */home/me/.config/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/me/.config/fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/me/.config/fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/me/.config/fzf/shell/key-bindings.bash"
