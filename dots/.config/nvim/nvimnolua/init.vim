runtime! archlinux.vim

" Theme
autocmd ColorScheme * highlight Normal ctermbg=NONE guibg=NONE

syntax enable
"set termguicolors
set mouse=a  
set t_Co=256
set wrap linebreak nolist
set expandtab
set nonumber
set relativenumber
set smarttab
set shiftround
"set tabstop=4  
set showmatch 
set ignorecase
set hlsearch
set incsearch
set updatetime=200
set spelllang=en_au
set splitright splitbelow
"set wildmenu
set autochdir
set scrolloff=10

" vimplug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'francoiscabrol/ranger.vim'
Plug 'rbgrouleff/bclose.vim'

call plug#end()
"let g:airline_theme='base16_default'
let g:airline_theme='monochrome'

" ranger
let g:ranger_map_keys = 0
let g:ranger_replace_netrw = 1 
" let g:ranger_command_override = 'ranger --cmd "set show_hidden=true"'

" fzf
let g:fzf_layout = { 'window': { "width": 0.6, "height": 0.6 } }
let $FZF_DEFAULT_OPTS = '--reverse'

" compile
autocmd BufWritePost config.def.h ! cp config.def.h config.h && sudo make clean install

" Keybinds
nmap Y y$
nmap '/  :e /<C-d>
nmap 'Q  :bd<cr>
nmap 'b  :Buffers<cr>
nmap ''  :b#<cr>
nmap '`  :FZF ~/<cr>
nmap 'c  :FZF ~/.config/<cr>
nmap 'j  :FZF ~/Sync/jots<cr>
nmap 'z  :e ~/.zshrc<cr>
nmap 'h  :e ~/.config/herbstluftwm/autostart<cr>
nmap 'q  :e ~/.config/qutebrowser/config.py<cr>
nmap 'd  :e ~/.config/suckless/dwm-6.2/config.def.h<cr>
nmap 'D  :e ~/.config/suckless/dmenu-5.0/config.def.h<cr>
nmap 'm  :e ~/.config/xmenu/xmenu.sh<cr>
nmap 'M  :MineSweep -t<cr>
nmap 'x  :e ~/.xinitrc<cr>
nmap 'X  :e ~/.Xresources<cr>
nmap 'r  :e ~/.ratpoisonrc<cr>
nmap 'v  :e ~/.config/nvim/init.vim<cr>
nmap 'g  :Goyo<cr>
nmap 'n  :set rnu!<cr>
nmap 'w  :vsp new<cr>
nmap 'g  :Goyo<CR>
nmap 'k  :<CR>
nmap 's  :set spell<CR>
nmap 'S  :set nospell<CR>
nmap <space>v :vs<CR>
nmap <space>h :split<CR>
nmap <space>k :WhichKey<CR>
nmap <space>a :Ranger<CR>

" Split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
