#ifndef CONFIG_H
#define CONFIG_H

#define MOD Mod4Mask

const char* term[]    = {"st",            0};
const char* menu[]    = {"dmenu_recent",  0};
const char* clipm[]   = {"clipmenu",      0};
const char* clipd[]   = {"clipd",         0};
const char* scrot[]   = {"scrot", "-s", "~/Pictures/scrot/'screen_%d-%m-%y_$wx$h.png'",      0};
const char* briup[]   = {"xbacklight", "-inc", "5", 0};
const char* bridown[] = {"xbacklight", "-dec", "5", 0};
const char* voldown[] = {"pamixer", "--allow-boost", "-d", "5",    0};
const char* volup[]   = {"pamixer", "--allow-boost", "-i", "5",    0};
const char* volmute[] = {"pamixer", "-t",           0};
const char* ranger[]  = {"st", "-e", "ranger",      0};
const char* nvim[]    = {"st", "-e", "nvim",        0};
const char* ncmpcpp[] = {"st", "-e", "ncmpcpp",     0};
const char* browser[] = {"google-chrome-stable",    0};
const char* configs[] = {"sh", "/home/me/.local/bin/configs",      0};
const char* search[]  = {"sh", "/home/me/.local/bin/search-engine", 0};
const char* toggle[]  = {"sh", "/home/me/.local/bin/toggle",       0};
const char* www[]     = {"sh", "/home/me/.local/bin/www",          0};
const char* power[]   = {"sh", "/home/me/.local/bin/powermenu.sh", 0};
const char* notify[]  = {"sh", "/home/me/.config/dunst/notify",    0};
const char* ram[]     = {"sh", "/home/me/.config/dunst/ram",       0};
const char* track[]   = {"sh", "/home/me/.config/dunst/track",     0};

static struct key keys[] = {
    {MOD,      XK_q,   win_kill,   {0}},
    {MOD,      XK_c,   win_center, {0}},
    {MOD,      XK_f,   win_fs,     {0}},

    {MOD,           XK_Up,    win_move,  {.com = (const char*[]){"move",   "n"}, .i = 30}},
    {MOD,           XK_Down,  win_move,  {.com = (const char*[]){"move",   "s"}, .i = 30}},
    {MOD,           XK_Right, win_move,  {.com = (const char*[]){"move",   "e"}, .i = 30}},
    {MOD,           XK_Left,  win_move,  {.com = (const char*[]){"move",   "w"}, .i = 30}},

    {MOD|ShiftMask, XK_Up,    win_move,  {.com = (const char*[]){"resize", "n"}, .i = 30}},
    {MOD|ShiftMask, XK_Down,  win_move,  {.com = (const char*[]){"resize", "s"}, .i = 30}},
    {MOD|ShiftMask, XK_Right, win_move,  {.com = (const char*[]){"resize", "e"}, .i = 30}},
    {MOD|ShiftMask, XK_Left,  win_move,  {.com = (const char*[]){"resize", "w"}, .i = 30}},

    {MOD,               XK_Tab, win_next,   {0}},
    {MOD|ShiftMask,     XK_Tab, win_prev,   {0}},

    {MOD,               XK_Return, run, {.com = term}},
    {MOD,               XK_space,  run, {.com = menu}},
    {MOD,               XK_y,      run, {.com = clipm}},
    {MOD|ShiftMask,     XK_y,      run, {.com = clipd}},
    {MOD,               XK_Print,  run, {.com = scrot}},
    {MOD,               XK_a,      run, {.com = ranger}},
    {MOD,               XK_v,      run, {.com = nvim}},
    {MOD,               XK_n,      run, {.com = ncmpcpp}},
    {MOD,               XK_w,      run, {.com = browser}},
    {MOD,               XK_slash,  run, {.com = configs}},
    {MOD,               XK_e,      run, {.com = search}},
    {MOD,               XK_o,      run, {.com = www}},
    {MOD,               XK_d,      run, {.com = toggle}},
    {MOD|ShiftMask,     XK_Delete, run, {.com = power}},
    {MOD,               XK_t,      run, {.com = notify}},
    {MOD,               XK_m,      run, {.com = ram}},
    {MOD,               XK_s,      run, {.com = track}},

    {0,   XF86XK_AudioLowerVolume,  run, {.com = voldown}},
    {0,   XF86XK_AudioRaiseVolume,  run, {.com = volup}},
    {0,   XF86XK_AudioMute,         run, {.com = volmute}},
    {0,   XF86XK_MonBrightnessUp,   run, {.com = briup}},
    {0,   XF86XK_MonBrightnessDown, run, {.com = bridown}},

    {MOD,           XK_1, ws_go,     {.i = 1}},
    {MOD|ShiftMask, XK_1, win_to_ws, {.i = 1}},
    {MOD,           XK_2, ws_go,     {.i = 2}},
    {MOD|ShiftMask, XK_2, win_to_ws, {.i = 2}},
    {MOD,           XK_3, ws_go,     {.i = 3}},
    {MOD|ShiftMask, XK_3, win_to_ws, {.i = 3}},
    {MOD,           XK_4, ws_go,     {.i = 4}},
    {MOD|ShiftMask, XK_4, win_to_ws, {.i = 4}},
    {MOD,           XK_5, ws_go,     {.i = 5}},
    {MOD|ShiftMask, XK_5, win_to_ws, {.i = 5}},
    {MOD,           XK_6, ws_go,     {.i = 6}},
    {MOD|ShiftMask, XK_6, win_to_ws, {.i = 6}},
};

#endif
