#!/bin/sh

cat <<EOF | xmenu | sh &
term	alacritty
radio
	wfmu	mpc clear; mpc load wfmu; mpc play
	drummer	mpc clear; mpc load drummer; mpc play
	ichiban	mpc clear; mpc load ichiban; mpc play
	CCX	mpc clear; mpc load CCX; mpc shuffle; mpc play
	stop	mpc toggle
scrot
	screen		sleep 2; scrot ~/Pictures/scrot/'screen_%d-%m-%y_$wx$h.png'
	selection	scrot -s ~/Pictures/scrot/'screen_%d-%m-%y_$wx$h.png'
	window		sleep 1; scrot -u ~/Pictures/scrot/'screen_%d-%m-%y_$wx$h.png'
exit
	logout		pkill dwm; pkill sowm
	lock		slock
	sleep		slock & systemctl suspend
	sleep in 20	sleep 1200 && systemctl suspend
	reboot		reboot
	poweroff	poweroff
EOF
