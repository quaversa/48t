#!/bin/bash

ARTIST=$(mpc current -f %artist%)
TRACK=$(mpc current -f %title%)
STATUS="playerctl status"

# if [ "$(playerctl status)" = "Playing" ]; then
#  playerctl -a metadata --format "{{ title }} - {{ artist }}"
if pgrep -x mpd; then
 echo "$ARTIST $TRACK "
else
 echo ""
fi

sleep 60
