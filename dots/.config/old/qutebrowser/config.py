
config.load_autoconfig(False)

c.aliases = {'q': 'close', 'qa': 'quit', 'w': 'session-save', 'wq': 'quit --save', 'wqa': 'quit --save'}

c.content.autoplay = False

config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')
config.set('content.headers.accept_language', '', 'https://matchmaker.krunker.io/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:71.0) Gecko/20100101 Firefox/71.0', 'https://docs.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:71.0) Gecko/20100101 Firefox/71.0', 'https://drive.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://web.whatsapp.com/')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version} Edg/{upstream_browser_version}', 'https://accounts.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')
config.set('content.images', True, 'chrome-devtools://*')
config.set('content.images', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome-devtools://*')
config.set('content.javascript.enabled', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')
config.set('content.register_protocol_handler', True, 'https://mail.google.com?extsrc=mailto&url=%25s')

c.content.mute = False
c.completion.height = '50%'
c.completion.scrollbar.width = 9
c.completion.open_categories = ['quickmarks', 'bookmarks', 'history', 'searchengines']
c.editor.command = ['st', '-e', 'nvim', '{}']
c.statusbar.show = 'never'

c.tabs.background = True
c.tabs.favicons.show = 'never'

# Valid values:
#   - ignore: Don't do anything.
#   - blank: Load a blank page.
#   - startpage: Load the start page.
#   - default-page: Load the default page.
#   - close: Close the window.
c.tabs.last_close = 'close'
c.tabs.new_position.related = 'next'
c.tabs.new_position.unrelated = 'last'
c.tabs.padding = {'bottom': 2, 'left': 15, 'right': 15, 'top': 2}
c.tabs.position = 'bottom'
c.tabs.show = 'multiple' #switching
c.tabs.show_switching_delay = 6000
c.tabs.title.alignment = 'center'
c.tabs.title.format = '{index}: {current_title}'
c.tabs.max_width = -1
c.tabs.indicator.width = 2
c.tabs.indicator.padding = {'bottom': 2, 'left': 0, 'right': 0, 'top': 2}

c.url.default_page = '~/.config/qutebrowser/termpage/index.html'

c.url.searchengines = {'DEFAULT': 'https://www.google.com.ar/search?q={}', 'bi': 'https://biqle.org/video/{}', 'd': 'https://duckduckgo.com/?q={}', 'eb': 'https://www.ebay.com.au/sch/{}', 'etym': 'https://www.etymonline.com/search?q={}', 'k': 'http://kat.rip/usearch/{}', 'g': 'https://www.google.com.ar/search?q={}', 'im': 'http://www.imdb.com/find?q={}', 'r': 'https://www.reddit.com/r/{}', 't': 'https://www.twitter.com/{}', 'w': 'https://en.wikipedia.org/wiki/{}', 'x': 'https://xhamster.com/search?q={}', 'y': 'https://www.youtube.com/results?search_query={}'}

c.url.start_pages = '~/.config/qutebrowser/termpage/index.html'

c.window.hide_decoration = True
c.window.title_format = '{perc}{current_title}{title_sep}'
c.window.transparent = True
c.zoom.default = '120%'
c.colors.completion.fg = '#9a9996'
c.colors.completion.odd.bg = '#1e222a'
c.colors.completion.even.bg = '#1e222a'
c.colors.completion.category.fg = '#d8dee9'
c.colors.completion.category.bg = '#1e222a'
c.colors.completion.category.border.top = '#1e222a'
c.colors.completion.category.border.bottom = '#1e222a'
c.colors.completion.item.selected.fg = '#9a9996'
c.colors.completion.item.selected.bg = '#1e222a'
c.colors.completion.item.selected.border.top = '#1e222a'
c.colors.completion.item.selected.border.bottom = '#1e222a'
c.colors.completion.item.selected.match.fg = '#ff6981'
c.colors.completion.match.fg = '#d8dee9'
c.colors.completion.scrollbar.fg = '#d8dee9'
c.colors.completion.scrollbar.bg = '#1e222a'

c.colors.downloads.bar.bg = '#1e222a'
c.colors.downloads.stop.bg = '#1e222a'
c.colors.downloads.system.bg = 'none'
c.colors.downloads.error.fg = '#e5e9f0'
c.colors.downloads.error.bg = '#bf616a'

c.colors.hints.fg = '#2e3440'
c.colors.hints.bg = '#ebcb8b'
c.colors.hints.match.fg = '#5e81ac'
c.colors.keyhint.fg = '#e5e9f0'
c.colors.keyhint.suffix.fg = '#ebcb8b'
c.colors.keyhint.bg = '#1e222a'

c.colors.messages.error.fg = '#e5e9f0'
c.colors.messages.error.bg = '#bf616a'
c.colors.messages.error.border = '#bf616a'
c.colors.messages.warning.fg = '#e5e9f0'
c.colors.messages.warning.bg = '#d08770'
c.colors.messages.warning.border = '#d08770'
c.colors.messages.info.fg = '#000000'
c.colors.messages.info.bg = '#88c0d0'
c.colors.messages.info.border = '#88c0d0'

c.colors.prompts.fg = '#e5e9f0'
c.colors.prompts.border = '1px solid #2e3440'
c.colors.prompts.bg = '#1e222a'
c.colors.prompts.selected.bg = '#1e222a'

c.colors.statusbar.normal.fg = '#9a9996'
c.colors.statusbar.normal.bg = '#1e222a'
c.colors.statusbar.insert.fg = '#9a9996'
c.colors.statusbar.insert.bg = '#1e222a'
c.colors.statusbar.passthrough.fg = '#1e222a'
c.colors.statusbar.passthrough.bg = '#1e222a'
c.colors.statusbar.private.fg = '#9a9996'
c.colors.statusbar.private.bg = '#1e222a'
c.colors.statusbar.command.fg = '#9a9996'
c.colors.statusbar.command.bg = '#1e222a'
c.colors.statusbar.command.private.fg = '#9a9996'
c.colors.statusbar.command.private.bg = '#1e222a'
c.colors.statusbar.caret.fg = '#9a9996'
c.colors.statusbar.caret.bg = '#1e222a'
c.colors.statusbar.caret.selection.fg = '#9a9996'
c.colors.statusbar.caret.selection.bg = '#1e222a'
c.colors.statusbar.progress.bg = '#1e222a'
c.colors.statusbar.url.fg = '#9a9996'
c.colors.statusbar.url.error.fg = '#9a9996'
c.colors.statusbar.url.hover.fg = '#9a9996'
c.colors.statusbar.url.success.http.fg = '#9a9996'
c.colors.statusbar.url.success.https.fg = '#9a9996'
c.colors.statusbar.url.warn.fg = '#d08770'

c.colors.tabs.bar.bg = '#1e222a'
c.colors.tabs.indicator.start = '#FF3B5A'
c.colors.tabs.indicator.stop = '#1e222a'
c.colors.tabs.indicator.error = '#bf616a'
c.colors.tabs.indicator.system = 'none'
c.colors.tabs.odd.fg = '#666666'
c.colors.tabs.odd.bg = '#1e222a'
c.colors.tabs.even.fg = '#666666'
c.colors.tabs.even.bg = '#1e222a'
c.colors.tabs.selected.odd.fg = '#d8dee9'
c.colors.tabs.selected.odd.bg = '#1e222a'
c.colors.tabs.selected.even.fg = '#d8dee9'
c.colors.tabs.selected.even.bg = '#1e222a'

c.colors.webpage.darkmode.enabled = False
c.colors.webpage.darkmode.algorithm = 'lightness-cielab'

c.fonts.default_family = 'Fira Code Nerd Font Complete Mono'
c.fonts.default_size = '15pt'

config.bind('ch', 'history-clear')
config.bind('za', 'spawn youtube-dl -o "~/Music/%(title)s.%(ext)s" -x --audio-quality 0 --audio-format "opus" --restrict-filenames {url} ```')
config.bind('zc', 'tab-give')
config.bind('zd', 'spawn youtube-dl -o "~/Videos/%(title)s.%(ext)s" --add-metadata -i {url}')
config.bind('zh', 'open qute://history')
config.bind('zl', 'hint links spawn mpv {hint-url}')
config.bind('zm', 'spawn mpv {url}')
config.bind('zp', 'open -p')
config.bind('zt', 'hint links spawn transmission-remote --add {hint-url}')
config.bind('zw', 'hint links spawn wget {url}')
config.bind('zx', 'tab-next')
config.bind('zz', 'tab-prev')

from qutebrowser.api import interceptor

def filter_yt(info: interceptor.Request):
  """Block the given request if necessary."""
  url = info.request_url
  if (url.host() == 'www.youtube.com' and
      url.path() == '/get_video_info' and
           '&adformat=' in url.query()):
      info.block()

interceptor.register(filter_yt)
