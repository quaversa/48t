#!/bin/sh
clear
hc() {
  herbstclient "$@"
}
# Create the layout directory if it doesn't exist
if [ ! -d ~/.config/herbstluftwm/layouts ];then
  mkdir ~/.config/herbstluftwm/layouts;
fi
# Create the layouts.old directory if it doesn't exist
if [ ! -d ~/.config/herbstluftwm/layouts.old ];then
  mkdir ~/.config/herbstluftwm/layouts.old;
fi
# name=$(ls ~/.config/herbstluftwm/layouts -1 | echo $(dmenu -l 20 -x 250 -y 250 -z 400 -p Delete))
name=$(ls ~/.config/herbstluftwm/layouts -1 | echo $(dmenu -p Delete))
mv ~/.config/herbstluftwm/layouts/$name ~/.config/herbstluftwm/layouts.old/$name
