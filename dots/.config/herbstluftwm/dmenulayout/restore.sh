#!/bin/sh
clear
# name=$(ls ~/.config/herbstluftwm/layouts -1 | echo $(dmenu -l 20 -x 250 -y 250 -z 400 -p Restore))
name=$(ls ~/.config/herbstluftwm/layouts -1 | echo $(dmenu -p Restore))
herbstclient load `herbstclient get_attr tags.focus.name` "$echo `cat ~/.config/herbstluftwm/layouts/$name`"
