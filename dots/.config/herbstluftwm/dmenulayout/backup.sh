#!/bin/sh
clear
hc() {
  herbstclient "$@"
}
# Create the layout directory if it doesn't exist
if [ ! -d ~/.config/herbstluftwm/layouts ];then
  mkdir ~/.config/herbstluftwm/layouts;
fi
# name=$(ls ~/.config/herbstluftwm/layouts -1 | echo $(dmenu -l 10 -x 250 -y 250 -z 400 -p Backup))
name=$(ls ~/.config/herbstluftwm/layouts -1 | echo $(dmenu -p Backup))
hc dump > ~/.config/herbstluftwm/layouts/$name
