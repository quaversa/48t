# initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

# Enable colors and change prompt:
autoload -U colors && colors
autoload -Uz tetriscurses

# Add .local/bin to PATH
if [ -d "$HOME/.local/bin" ]; then;
		PATH="$HOME/.local/bin:$PATH"
fi
export CDPATH=.:~:/media:~/.config:~/Pictures:~/.config/suckless

PS1="%~ %{$fg[red]%}%{$reset_color%}$%b "

HISTFILE=~/.histfile
HISTSIZE=2000
SAVEHIST=2000

setopt HIST_VERIFY
setopt SHARE_HISTORY
setopt HIST_IGNORE_SPACE
setopt HIST_SAVE_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
export MANWIDTH=120
setopt HIST_IGNORE_SPACE
setopt extendedglob
unsetopt beep
export KEYTIMEOUT=1
zstyle :compinstall filename '/home/me/.zshrc'
setopt HIST_IGNORE_SPACE

export EDITOR="nvim"
export VISUAL="nvim"
export TERMINAL="st"
export BROWSER="qutebrowser"
export READER="zathura"
export FILE="ranger"
export TUIR_EDITOR="nvim"
export TUIR_BROWSER="qutebrowser"

export FZF_DEFAULT_COMMAND="find -L -maxdepth 4"
export BW_SESSION="n3oDG6qHGZbx9nFXYfYR4GL+llGIn+NmJUwb580B2bcut7Y8Rxct5aNvB34UhPZKqzodRPSkrc1TbZuRIn+OSw=="
export PATH="$HOME/.local/bin:$PATH"
export HISTIGNORE="ytfzf*:yt*:youtube-dl*"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export DEFAULT_RECIPIENT="quaoub@gmail.com"

export CM_LAUNCHER='dmenu'
export CM_MAX_CLIPS=24

export NNN_PLUG='c:cmusq;e:togglex;d:dragdrop;f:fzcd;i:imgview;m:nmount;l:preview-tui;s:suedit;u:upload;w:wallpaper;x:xdgdefault'
export NNN_FCOLORS='0000E6310000000000000000'
export NNN_FIFO="/tmp/nnn.fifo"
export NNN_TRASH=n=1
export NNN_MCLICK='.'
export NNN_BMS='b:~/.local/bin;c:~/.config;d:~/Downloads;g:~/Git;h:~/;j:~/Sync/jots;n:~/.config/nnn;p:~/Pictures;s:~/.config/suckless;t:~/Templates;u:/media;v:~/Videos/fliks'

bindkey "^[b" backward-word 
bindkey "^[w" forward-word

#dynamic terminal title
case $TERM in
  (*alacritty* | st)
    function precmd {
      print -Pn "\e]0; %(1j,%j job%(2j|s|); ,)%~\a"
    }
    function preexec {
      printf "\033]0;%s\a" "$1"
    }
  ;;
esac

# Rangercd
rangercd () {
    tmp="$(mktemp)"
    ranger --choosedir="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ --datadir "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"                                               
    fi
}

cd() # cd and ls after
{
	builtin cd "$@" && command exa --group-directories-first --color-scale
}

por() # remove orphaned packages
{
	local orphans=($(pacman -Qtdq 2>/dev/null))
	if (( ${#orphans[@]} == 0 )); then
		printf "System has no orphaned packages\n"
	else
		sudo pacman -Rns "${orphans[@]}"
	fi
}

arc() # create and extract archives
{
	if [[ $# -eq 0 || $1 =~ (--help|-h) ]]; then
		printf "usage: arc [-e, --extract] <archive_name>\n"
		printf "       arc [-n, --new] <name.type> files...\n"
		printf "\n\nwhen first argument is an archive --ext is assumed\n"
		return 0
	fi

	(( $# >= 2 )) && { arg="$1"; shift; } || arg='-e'

	case "$arg" in
		-e|--extract)
			if [[ "$1" && -e "$1" ]]; then
				case "$1" in
					*.tbz2|*.tar.bz2) tar xvjf "$1" ;;
					*.tgz|*.tar.gz) tar xvzf "$1" ;;
					*.tar.xz) tar xpvf "$1" ;;
					*.tar) tar xvf "$1" ;;
					*.gz) gunzip "$1" ;;
					*.zip) unzip "$1" ;;
					*.bz2) bunzip2 "$1" ;;
					*.7zip) 7za e "$1" ;;
					*.rar) unrar x "$1" ;;
					*.deb) { ar p "$1" data.tar.xz | tar x; } || { ar p "$1" data.tar.gz | tar xz; } ;;
					*) printf "invalid archive for extraction: %s" "$1"; return 1
				esac
			else
				printf "invalid archive for extraction: %s" "$1"; return 1
			fi ;;
		-n|--new)
			case "$1" in
				*.tar.*)
					n="${1%.*}" e="${1#*.tar.}"; shift
					tar cvf "$n" "$@" || return 1
					case "$e" in
						xz) xz "$n" ;;
						gz) gzip -9r "$n" ;;
						bz2) bzip2 -9zv "$n" ;;
						*) printf "invalid extension after tar %s" "$e"; return 1
					esac ;;
				*.gz) shift; gzip -9rk "$@" ;;
				*.zip) zip -9r "$@" ;;
				*.7z) 7z a -mx9 "$@" ;;
				*) printf "invalid extension %s" "$1"; return 1
			esac ;;
		*) printf "invalid argument %s (use --help)" "$arg"; return 1
	esac
}

##aliases
. ~/.config/zsh/zsh_aliases
##fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
#fsysctl
source /usr/share/zsh/plugins/fuzzy-sys.plugin.zsh 
#autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
#zsh-syntax-highlighting; should be last
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
