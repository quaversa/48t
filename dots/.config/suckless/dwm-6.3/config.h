/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 12;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "Hack Nerd Font Mono:style=Medium:pixelsize=21:antialias=true:autohint=true" };
static const char dmenufont[]       = "Hack Nerd Font Mono:style=Medium:pixelsize=22:antialias=true:autohint=true";
static const char col_black[]       = "#000000";
static const char col_gray[]        = "#888888";
static const char col_blue[]        = "#1e222a";
static const char col_green[]       = "#1e333a";
static const char col_white[]       = "#bbbbbb";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray, col_blue,  col_blue },
	[SchemeSel]  = { col_white, col_green,  col_green },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       1 << 4,       0,           -1 },
  { "mpv",      NULL,       NULL,       1 << 1,       0,           -1 }, 
	// { "mpv",      NULL,       NULL,       1,            0,            1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ ">^<",      NULL },    /* no layout function means floating behavior */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_recent", "-m", dmenumon, "-fn", dmenufont, "-nb", col_blue, "-nf", col_gray, "-sb", col_green, "-sf", col_white, NULL };
static const char *clipmenucmd[] = { "clipmenu", "-m", dmenumon, "-fn", dmenufont, "-nb", col_blue, "-nf", col_gray, "-sb", col_green, "-sf", col_white, NULL };
static const char *termcmd[]  	= { "st", NULL };

#include <X11/XF86keysym.h>

static Key keys[] = {
	/* modifier                     key             function  argument */
	{ MODKEY,                       XK_space,	      spawn,		{.v = dmenucmd } },
	{ MODKEY,                       XK_Return,      spawn,		{.v = termcmd } },
	{ MODKEY,                       XK_y,		        spawn,		{.v = clipmenucmd } },
	{ MODKEY,	                    	XK_backslash,	  spawn,		SHCMD("st -g 130x32+110+80") },
	{ MODKEY|ShiftMask,            	XK_backslash,	  spawn,		SHCMD("st -g 130x32+110+80 -e ranger") },
	{ MODKEY|ShiftMask,           	XK_y,		        spawn,		SHCMD("sh ~/.local/bin/clipd") },
 	{ MODKEY,			                  XK_w,	        	spawn,		SHCMD("firefox") },
 	{ MODKEY|ShiftMask,     	      XK_w,	        	spawn,		SHCMD("firefox --private-window") },
 	{ MODKEY,			                  XK_v,	        	spawn,		SHCMD("st -g 125x34+150+80 -e nvim") },
	{ MODKEY,			                  XK_a,	        	spawn,		SHCMD("st -g 90x25+0+0 -e ranger") },
	{ MODKEY|ShiftMask,		          XK_a,	        	spawn,		SHCMD("st -g 90x25+0+0 -e sudo ranger") },
	{ Mod1Mask,			                XK_a,	        	spawn,		SHCMD("st -g 28x28+0+0 -e nnn -neH") },
	{ MODKEY,			                  XK_n,	        	spawn,		SHCMD("st -g 125x25+140+50 -e ncmpcpp") },
	{ MODKEY|ShiftMask,		          XK_n,	        	spawn,		SHCMD("mpd") },
 	{ MODKEY,			                  XK_p,	        	spawn,		SHCMD("st -g 87x8+380+0 -e pulsemixer") },
 	{ MODKEY|ShiftMask,		          XK_p,	        	spawn,		SHCMD("st -g 87x8+380+0 -e ncpamixer") },
 	{ MODKEY,       		            XK_c,	        	spawn,		SHCMD("st -g 110x11+245+10 -e cointop --hide-chart") },
	{ MODKEY|ShiftMask,            	XK_q,          	spawn,		SHCMD("sh ~/.local/bin/power") },
	{ MODKEY|ShiftMask,            	XK_Delete,    	spawn,		SHCMD("sh ~/.local/bin/power") },
	{ MODKEY,            	          XK_x,         	spawn,		SHCMD("sh ~/.config/xmouseless/xmouseless") },
	{ Mod1Mask,	                   	XK_b,		        spawn,		SHCMD("sh ~/.local/bin/bluetooth") },
	{ Mod1Mask,	                   	XK_w,		        spawn,		SHCMD("sh ~/.local/bin/wifi") },
	{ Mod1Mask,	                   	XK_d,   	      spawn,		SHCMD("sh ~/.local/bin/daemons") },
	{ MODKEY,	                    	XK_e,   	      spawn,		SHCMD("sh ~/.local/bin/places") },
	{ MODKEY,	                    	XK_o,	        	spawn,		SHCMD("sh ~/.local/bin/www") },
	{ Mod1Mask,	                  	XK_t,	        	spawn,		SHCMD("st -g 110x30+235+100 -e tuir") },
	{ Mod1Mask,	                  	XK_m,	        	spawn,		SHCMD("sh ~/.local/bin/clipmpv") },
	{ MODKEY|ShiftMask,           	XK_d,	        	spawn,		SHCMD("st -g 95x25+340+250 -e dict") },
	{ MODKEY|ShiftMask,     	      XK_slash,     	spawn,		SHCMD("sh ~/.local/bin/search-engine") },
	{ MODKEY,	            	        XK_slash,     	spawn,		SHCMD("sh ~/.local/bin/configs") },
	{ MODKEY,			                  XK_Print,     	spawn,	  SHCMD("scrot -s ~/Pictures/scrot/'screen_%d-%m-%y_$wx$h.png'") },
	{ 0,				                    XK_Print,     	spawn,    SHCMD("scrot ~/Pictures/scrot/'screen_%d-%m-%y_$wx$h.png'") },
	{ 0,                 	XF86XK_MonBrightnessUp,		spawn,		SHCMD("xbacklight -inc 5") },
	{ 0,               		XF86XK_MonBrightnessDown,	spawn,		SHCMD("xbacklight -dec 5") },
  { 0,                  XF86XK_AudioMute,		      spawn,   	SHCMD("pamixer -t") },
	{ 0,                  XF86XK_AudioRaiseVolume,	spawn,   	SHCMD("pamixer --allow-boost -i 5") },
	{ 0,                  XF86XK_AudioLowerVolume,	spawn,   	SHCMD("pamixer --allow-boost -d 5") },
	{ 0,                	XF86XK_Tools,	        	  spawn,		SHCMD("sh ~/.local/bin/dmpdu") },
	{ 0,                  XF86XK_Favorites,		      spawn,		SHCMD("sh ~/.local/bin/woofmoo") },
	{ 0,                 	XF86XK_Display, 		      spawn,		SHCMD("xrandr --auto") },
	{ MODKEY,                       XK_b,           togglebar,      {0} },
	{ MODKEY,                       XK_Tab,         focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_j,           focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,           focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,           incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,           incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,           setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,           setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return,      zoom,           {0} },
	{ MODKEY,                       XK_grave,       view,           {0} },
	{ MODKEY,                       XK_q,           killclient,     {0} },
	{ MODKEY,                       XK_f,           setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_t,           setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,           setlayout,      {.v = &layouts[2]} },
	{ Mod1Mask,                     XK_space,       setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,       togglefloating, {0} },
	{ MODKEY,                       XK_0,           view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,           tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,       focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,      focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,       tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,      tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                           0)
	TAGKEYS(                        XK_2,                           1)
	TAGKEYS(                        XK_3,                           2)
	TAGKEYS(                        XK_4,                           3)
	TAGKEYS(                        XK_5,                           4)
	{ MODKEY|ControlMask|ShiftMask, XK_q,           quit,          {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button1,        zoom,           {0} },
	{ ClkWinTitle,          0,              Button2,        killclient,     {0} },
	{ ClkRootWin,           0,              Button2,        spawn,          SHCMD("sh ~/.local/bin/daemons") },  
  { ClkStatusText,        0,              Button1,        spawn,          SHCMD("pamixer --allow-boost -d 5") },
  { ClkStatusText,        0,              Button2,        spawn,          SHCMD("pamixer -t") }, 
	{ ClkStatusText,        0,              Button3,        spawn,          SHCMD("pamixer --allow-boost -i 5") }, 
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
