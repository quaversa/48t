/* user and group to drop privileges to */
static const char *user  = "me";
static const char *group = "me";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#006060",     /* after initialization */
	[INPUT] =  "#69448e",   /* during input */
	[FAILED] = "#CC3333",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
